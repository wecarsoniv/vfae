# Variational Fair Autoencoder Implementation


## Introduction

This project is a PyTorch implementation of the proposed model from the paper [*Variational Fair Autoencoder* by Louizos et al. (2015)](https://arxiv.org/abs/1511.00830).


## Technologies

This project relies on the following libaries and technologies:

* Python >= 3.6
* PyTorch >= 1.6

Only key libraries and minimum version numbers are listed, other libraries and packages and their version numbers are omitted for brevity.

## Files

Files related to the implementation of the [Variational Autoencoder, proposed by Kingma and Welling (2013)](https://arxiv.org/abs/1312.6114) can be found in the `vae/` directory, and files related to the implementation of the Variational Fair Autoencoder can be found in the `vfae/` directory. 


### Variational Autoencoder

The file `vae_model.py` contains the PyTorch implementation of the Variational Autoencoder. The file `vae_loss.py` contains the implementation of the loss function for the Variational Autoencoder.


### Variational Fair Autoencoder

The file `vfae_model.py` contains the PyTorch implementation of the Variational Fair Autoencoder model, and the file `vfae_loss.py` contains the implementation of the loss function for the Variational Fair Autoencoder. Implementation code was modified from [code written by GitHub user yolomeus](https://github.com/yolomeus/DMLab2020VFAE).

