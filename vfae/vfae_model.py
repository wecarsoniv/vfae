# ----------------------------------------------------------------------------------------------------------------------
# FILE DESCRIPTION
# ----------------------------------------------------------------------------------------------------------------------

# File:  vfae_model.py
# Author:  Billy Carson
# Date written:  07-15-2020
# Last modified:  07-22-2020

"""
Description:  PyTorch implementation of the Variational Fair Autoencoder proposed by Louizos et al., 2015.
"""


# ----------------------------------------------------------------------------------------------------------------------
# IMPORT MODULES
# ----------------------------------------------------------------------------------------------------------------------

# Import modules
import torch
import torch.nn as nn


# ----------------------------------------------------------------------------------------------------------------------
# DEFINE CLASSES
# ----------------------------------------------------------------------------------------------------------------------

# Semi-supervised Variational Fair Autoencoder class
class sVFAE(nn.Module):
    """
    Description
    -----------
    PyTorch implementation of the supervised version of the Variational Fair Autoencoder (Louizos et al., 2015).

    Parameters
    ----------
    x_size :  int; primary feature dimensionality
    s_size :  int; sensitive feature dimensionality
    y_size :  int; label dimensionality
    z_size :  int; latent variable dimensionality
    z1_enc_hidden_size :  int; z1 encoder hidden layer size
    z2_enc_hidden_size :  int; z2 encoder hidden layer size
    x_dec_hidden_size :  int; primary variable decoder hidden layer size
    z1_dec_hidden_size :  int; z1 decoder hidden layer size
    hidden_activation :  torch.nn.modules.activation; hidden layer output activation function
    output_activation :  torch.nn.modules.activation; decoder output activation function

    Attributes
    ----------
    x_size :  int; primary feature dimensionality
    s_size :  int; sensitive feature dimensionality
    y_size :  int; label dimensionality
    z_size :  int; latent variable dimensionality
    z1_enc_hidden_size :  int; z1 encoder hidden layer size
    z2_enc_hidden_size :  int; z2 encoder hidden layer size
    x_dec_hidden_size :  int; primary variable decoder hidden layer size
    z1_dec_hidden_size :  int; z1 decoder hidden layer size
    z1_encoder :  torch.nn.Module(); variational multi-layer neural network z1 encoder
    z2_encoder :  torch.nn.Module(); variational multi-layer neural network z2 encoder
    z1_decoder :  torch.nn.Module(); variational multi-layer neural network z1 decoder
    x_decoder :  torch.nn.Module(); variational multi-layer neural network x decoder
    y_decoder :  torch.nn.Module(); variational multi-layer neural network y decoder
    hidden_activation :  torch.nn.modules.activation; hidden layer output activation function
    output_activation :  torch.nn.modules.activation; decoder output activation function
    """

    # Initializer method
    def __init__(self, x_size, s_size, y_size, z_size=10, z1_enc_hidden_size=20, z2_enc_hidden_size=20,
                 x_dec_hidden_size=None, z1_dec_hidden_size=None, hidden_activation=nn.ReLU(inplace=False),
                 output_activation=nn.Sigmoid()):
        """
        Description
        -----------
        Initializer method for the supervised version of the Variational Fair Autoencoder (Louizos et al., 2015).

        Parameters
        ----------
        x_size :  int; primary feature dimensionality
        s_size :  int; sensitive feature dimensionality
        y_size :  int; label dimensionality
        z_size :  int; latent variable dimensionality
        z1_enc_hidden_size :  int; z1 encoder hidden layer size
        z2_enc_hidden_size :  int; z2 encoder hidden layer size
        x_dec_hidden_size :  int; primary variable decoder hidden layer size
        z1_dec_hidden_size :  int; z1 decoder hidden layer size
        hidden_activation :  torch.nn.modules.activation; hidden layer output activation function
        output_activation :  torch.nn.modules.activation; decoder output activation function

        Returns
        -------
        N/A
        """

        # Inherit from torch.nn.Module
        super(sVFAE, self).__init__()

        # Assign attributes
        self.x_size = x_size
        self.s_size = s_size
        self.z_size = z_size
        self.y_size = y_size
        self.x_dec_hidden_size = x_dec_hidden_size
        self.z1_enc_hidden_size = z1_enc_hidden_size
        self.z2_enc_hidden_size = z2_enc_hidden_size
        self.z1_dec_hidden_size = z1_dec_hidden_size
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation

        # Create encoders
        self.encoder_z1 = VariationalMLP(input_size=x_size + s_size, hidden_size=z1_enc_hidden_size,
                                         latent_size=z_size, hidden_activation=hidden_activation)
        self.encoder_z2 = VariationalMLP(input_size=z_size + y_size, hidden_size=z2_enc_hidden_size,
                                         latent_size=z_size, hidden_activation=hidden_activation)

        # Create decoders
        self.decoder_z1 = VariationalMLP(input_size=z_size + y_size, hidden_size=z2_enc_hidden_size,
                                         latent_size=z_size, hidden_activation=hidden_activation)
        if x_dec_hidden_size is None:
            self.decoder_x = DecoderLinear(latent_size=z_size + s_size, output_size=x_size + s_size,
                                           output_activation=output_activation)
        else:
            self.decoder_x = DecoderMLP(latent_size=z_size + s_size, hidden_size=x_dec_hidden_size,
                                        output_size=x_size + s_size, hidden_activation=hidden_activation,
                                        output_activation=output_activation)
        if z1_dec_hidden_size is None:
            self.decoder_y = DecoderLinear(latent_size=z_size, output_size=y_size, output_activation=output_activation)
        else:
            self.decoder_y = DecoderMLP(latent_size=z_size, hidden_size=x_dec_hidden_size, output_size=y_size,
                                        hidden_activation=hidden_activation, output_activation=output_activation)

    # Forward method
    def forward(self, x, s, y):
        """
        Description
        -----------
        Model forward method.

        Parameters
        ----------
        x :  torch.Tensor(); batch tensor of primary variables to encode
        s : torch.Tensor(); batch tensor of sensitive variables
        y : torch.Tensor(); batch tensor of labels

        Returns
        -------
        outputs :  dict; dictionary of output tensors
        """

        # Encode x and s into latent representation z1
        x_s = torch.cat(tensors=(x, s), dim=1)
        z1_enc, z1_enc_mu, z1_enc_logvar = self.encoder_z1(x_s)

        # Encode z1 and y into latent representation z2
        z1_y = torch.cat(tensors=(z1_enc, y), dim=1)
        z2_enc, z2_enc_mu, z2_enc_logvar = self.encoder_z2(z1_y)

        # Reconstruct z1 from z2 and y
        z2_y = torch.cat(tensors=(z2_enc, y), dim=1)
        z1_dec, z1_dec_logvar, z1_dec_mu = self.decoder_z1(z2_y)

        # Reconstruct x from z1 reconstruction and s
        z1_s = torch.cat(tensors=(z1_dec, s), dim=1)
        x_dec = self.decoder_x(z1_s)

        # Reconstruct y from z1
        y_dec = self.decoder_y(z1_enc)

        # Dictionary of outputs
        outputs = {
            'x_dec': x_dec,
            'y_dec': y_dec,
            'z1_enc': z1_enc,
            'z1_enc_mu': z1_enc_mu,
            'z1_enc_logvar': z1_enc_logvar,
            'z2_enc_mu': z2_enc_mu,
            'z2_enc_logvar': z2_enc_logvar,
            'z1_dec_mu': z1_dec_mu,
            'z1_dec_logvar': z1_dec_logvar}

        # Return outputs
        # return x_dec, y_dec, z1_enc, z1_enc_mu, z1_enc_logvar, z2_enc_mu, z2_enc_logvar, z1_dec_mu, z1_dec_logvar
        return outputs

    # Encoder method
    def encode(self, x, s):
        """
        Description
        -----------
        Encoder method that returns encoding of latent invariant features.

        Parameters
        ----------
        x :  torch.Tensor(); batch tensor of primary variables to encode
        s : torch.Tensor(); batch tensor of sensitive variables

        Returns
        -------
        z1_enc :  torch.Tensor(); batch tensor of latent invariant encodings
        """

        # Encode x and s into latent representation z1
        x_s = torch.cat(tensors=(x, s), dim=1)
        z1_enc, z1_enc_mu, z1_enc_logvar = self.encoder_z1(x_s)

        # Return latent encoding
        return z1_enc


# Unsupervised Variational Fair Autoencoder class
class uVFAE(nn.Module):
    """
    Description
    -----------
    Initializer method for the unsupervised version of the Variational Fair Autoencoder (Louizos et al., 2015).

    Parameters
    ----------
    x_size :  int; primary feature dimensionality
    s_size :  int; sensitive feature dimensionality
    z_size :  int; latent variable dimensionality
    z_enc_hidden_size :  int; z encoder hidden layer size
    x_dec_hidden_size :  int; primary variable decoder hidden layer size
    hidden_activation :  torch.nn.modules.activation; hidden layer output activation function
    output_activation :  torch.nn.modules.activation; output activation function

    Attributes
    ----------
    x_size :  int; primary feature dimensionality
    s_size :  int; sensitive feature dimensionality
    z_size :  int; latent variable dimensionality
    z_enc_hidden_size :  int; z encoder hidden layer size
    x_dec_hidden_size :  int; primary variable decoder hidden layer size
    z_encoder :  torch.nn.Module(); variational multi-layer neural network z encoder
    x_decoder :  torch.nn.Module(); variational multi-layer neural network x decoder
    hidden_activation :  torch.nn.modules.activation; hidden layer output activation function
    output_activation :  torch.nn.modules.activation; decoder output activation function
    """

    # Initializer method
    def __init__(self, x_size, s_size, y_size, z_size=10, z_enc_hidden_size=20, x_dec_hidden_size=20,
                 hidden_activation=nn.ReLU(), output_activation=nn.Sigmoid()):
        """
        Description
        -----------
        Initializer method for the supervised version of the Variational Fair Autoencoder (Louizos et al., 2015).

        Parameters
        ----------
        x_size :  int; primary feature dimensionality
        s_size :  int; sensitive feature dimensionality
        z_size :  int; latent variable dimensionality
        z_enc_hidden_size :  int; z encoder hidden layer size
        x_dec_hidden_size :  int; primary variable decoder hidden layer size
        hidden_activation :  torch.nn.modules.activation; hidden layer output activation function
        output_activation :  torch.nn.modules.activation; decoder output activation function

        Returns
        -------
        N/A
        """

        # Inherit from torch.nn.Module
        super(uVFAE, self).__init__()

        # Assign attributes
        self.x_size = x_size
        self.s_size = s_size
        self.z_size = z_size
        self.y_size = y_size
        self.z_enc_hidden_size = z_enc_hidden_size
        self.x_dec_hidden_size = x_dec_hidden_size
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation

        # Create encoders
        self.z_encoder = VariationalMLP(input_size=x_size + s_size, hidden_size=z_enc_hidden_size,
                                        latent_size=z_size, hidden_activation=hidden_activation)

        # Create decoders
        self.x_decoder = DecoderMLP(latent_size=z_size + s_size, hidden_size=x_dec_hidden_size,
                                    output_size=x_size + s_size, hidden_activation=hidden_activation,
                                    output_activation=output_activation)

    # Forward method
    def forward(self, x, s):
        """
        Description
        -----------
        Model forward method.

        Parameters
        ----------
        x :  torch.Tensor(); batch tensor of primary variables to encode
        s :  torch.Tensor(); batch tensor of sensitive variables
        y :  torch.Tensor(); batch tensor of labels
        Returns
        -------
        outputs :  dict; dictionary of output tensors
        """

        # Encode x and s into latent representation z
        x_s = torch.cat(tensors=(x, s), dim=1)
        z_enc, z_enc_mu, z_enc_logvar = self.z_encoder(x_s)

        # Reconstruct x from z and s
        z_s = torch.cat(tensors=(z_enc, s), dim=1)
        x_dec = self.decoder_x(z_s)

        # Dictionary of outputs
        outputs = {
            'x_dec': x_dec,
            'z_enc': z_enc,
            'z_enc_mu': z_enc_mu,
            'z_enc_logvar': z_enc_logvar}

        # Return outputs
        # return x_dec, z_enc, z_enc_mu, z_enc_logvar
        return outputs

    # Encoder method
    def encode(self, x, s):
        """
        Description
        -----------
        Encoder method that returns encoding of latent invariant features.

        Parameters
        ----------
        x :  torch.Tensor(); batch tensor of primary variables to encode
        s :  torch.Tensor(); batch tensor of sensitive variables

        Returns
        -------
        z_enc :  torch.Tensor(); batch tensor of latent invariant encodings
        """

        # Encode x and s into latent representation z
        x_s = torch.cat(tensors=(x, s), dim=1)
        z_enc, z_enc_mu, z_enc_logvar = self.z_encoder(x_s)

        # Return latent encoding
        return z_enc


# Variational encoder class
class VariationalMLP(nn.Module):
    """
    Description
    -----------
    Variational multi-layer neural network class.

    Parameters
    ----------
    input_size :  int; input layer size
    hidden_size :  int; hidden layer size
    latent_size :  int; latent layer size
    hidden_activation :  torch.nn.modules.activation; hidden layer output activation function

    Attributes
    ----------
    fc_hidden :  torch.nn.Module(); input to hidden fully-connected layer
    fc_mu :  torch.nn.Module(); hidden to mean fully-connected layer
    fc_logvar :  torch.nn.Module(); hidden to log-variance fully-connected layer
    hidden_activation :  torch.nn.modules.activation; hidden layer output activation function
    """

    # Encoder initializer method
    def __init__(self, input_size, hidden_size, latent_size, hidden_activation=nn.ReLU(inplace=False)):
        """
        Description
        -----------
        Variational multi-layer neural network initializer method.

        Parameters
        ----------
        input_size :  int; input layer size
        hidden_size :  int; hidden layer size
        latent_size :  int; latent layer size
        hidden_activation :  torch.nn.modules.activation; hidden layer output activation function

        Returns
        -------
        N/A
        """

        # Super
        super().__init__()

        # Create variational encoder
        self.fc_hidden = nn.Linear(in_features=input_size, out_features=hidden_size)
        self.fc_mu = nn.Linear(in_features=hidden_size, out_features=latent_size)
        self.fc_logvar = nn.Linear(in_features=hidden_size, out_features=latent_size)
        self.hidden_activation = hidden_activation

    # Forward method
    def forward(self, x):
        """
        Description
        -----------
        Variational multi-layer neural network forward method.

        Parameters
        ----------
        x :  torch.Tensor(); batch tensor of inputs

        Returns
        -------
        z :  torch.Tensor(); batch tensor of latent encodings
        mu :  torch.Tensor(); batch tensor of mean values
        logvar :  torch.Tensor(); batch tensor of log-variance values
        """

        # Get mu and log-variance
        x = self.fc_hidden(x)
        x = self.hidden_activation(x)
        z_mu = self.fc_mu(x)
        z_logvar = self.fc_logvar(x)

        # Reparameterization trick
        z = self._reparametrize(mu=z_mu, logvar=z_logvar)

        # Return output
        return z, z_mu, z_logvar

    # Reparameterization trick method
    @staticmethod
    def _reparametrize(mu, logvar):
        """
        Description
        -----------
        Reparameterization trick method.

        Parameters
        ----------
        mu :  torch.Tensor(); tensor of mean values
        logvar :  torch.Tensor; tensor of log-variance values

        Returns
        -------
        z :  torch.Tensor(); tensor of latent encoding values
        """

        # Reparameterization trick
        std = torch.exp(0.5 * logvar)
        epsilon = torch.randn_like(std)
        z = mu + (epsilon * std)

        # Return z
        return z


# Decoder class
class DecoderMLP(nn.Module):
    """
    Description
    -----------
    Decoder multi-layer neural network class.

    Parameters
    ----------
    latent_size :  int; latent layer size
    hidden_size :  int; hidden layer size
    output_size :  int; output layer size

    Attributes
    ----------
    fc_hidden :  torch.nn.Module(); latent to hidden fully-connected layer
    fc_output :  torch.nn.Module(); hidden to output fully-connected layer
    """

    # Decoder initializer method
    def __init__(self, latent_size, hidden_size, output_size, hidden_activation=nn.ReLU(inplace=False),
                 output_activation=None):
        """
        Description
        -----------
        Decoder multi-layer neural network initializer method.

        Parameters
        ----------
        latent_size :  int; latent layer size
        hidden_size :  int; hidden layer size
        output_size :  int; output layer size
        hidden_activation :  torch.nn.modules.activation; hidden layer output activation function
        output_activation :  torch.nn.modules.activation; decoder output activation function

        Returns
        -------
        N/A
        """

        # Inherit from
        super().__init__()

        # Decoder model
        self.fc_hidden = nn.Linear(in_features=latent_size, out_features=hidden_size)
        self.fc_output = nn.Linear(in_features=hidden_size, out_features=output_size)
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation

    # Forward method
    def forward(self, x):
        """
        Description
        -----------
        Decoder multi-layer neural network forward method.

        Parameters
        ----------
        x :  torch.Tensor(); batch tensor of latent encodings

        Returns
        -------
        output :  torch.Tensor(); batch tensor of decoded outputs
        """

        # Forward
        x = self.fc_hidden(x)
        x = self.hidden_activation(x)
        output = self.fc_output(x)

        # Output activation
        if self.output_activation is not None:
            output = self.output_activation(output)

        # Return output
        return output


# Linear decoder class
class DecoderLinear(nn.Module):
    """
    Description
    -----------
    Linear decoder class.

    Parameters
    ----------
    latent_size :  int; latent layer size
    output_size :  int; output layer size

    Attributes
    ----------
    fc_output :  torch.nn.Module(); hidden to output fully-connected layer
    """

    # Decoder initializer method
    def __init__(self, latent_size, output_size, output_activation=None):
        """
        Description
        -----------
        Decoder multi-layer neural network initializer method.

        Parameters
        ----------
        latent_size :  int; latent layer size
        output_size :  int; output layer size
        output_activation :  torch.nn.modules.activation; decoder output activation function

        Returns
        -------
        N/A
        """

        # Inherit from
        super().__init__()

        # Decoder model
        self.fc_output = nn.Linear(in_features=latent_size, out_features=output_size)
        self.output_activation = output_activation

    # Forward method
    def forward(self, x):
        """
        Description
        -----------
        Decoder multi-layer neural network forward method.

        Parameters
        ----------
        x :  torch.Tensor(); batch tensor of latent encodings

        Returns
        -------
        output :  torch.Tensor(); batch tensor of decoded outputs
        """

        # Forward
        output = self.fc_output(x)

        # Output activation function
        if self.output_activation is not None:
            output = self.output_activation(output)

        # Return output
        return output

