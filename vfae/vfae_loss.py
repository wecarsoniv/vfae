# ----------------------------------------------------------------------------------------------------------------------
# FILE DESCRIPTION
# ----------------------------------------------------------------------------------------------------------------------

# File: vfae_loss.py
# Author: Billy Carson
# Date written: 07-15-2020
# Last modified: 07-22-2020

"""
Description:  PyTorch implementation of the Variational Fair Autoencoder proposed by Louizos et al., 2015.
"""


# ----------------------------------------------------------------------------------------------------------------------
# IMPORT MODULES
# ----------------------------------------------------------------------------------------------------------------------

# Import modules
import numpy as np
import pandas as pd
from math import sqrt, pi
import torch
from torch import sum, exp, square, cos, zeros_like
import torch.nn as nn
from torch.nn import CrossEntropyLoss, BCEWithLogitsLoss


# ----------------------------------------------------------------------------------------------------------------------
# DEFINE CLASSES
# ----------------------------------------------------------------------------------------------------------------------

# Semi-supervised Variational Fair Autoencoder loss
class sVFAELoss(nn.modules.loss._Loss):
    """
    Description
    -----------
    Semi-supervised Variational Fair Autoencoder loss class.

    Parameters
    ----------
    mmd :  bool; indicates whether to use Maximum Mean Discrepancy penalty
    reduction :  str; string indicating type of reduction to perform

    Attributes
    ----------
    mmd :  bool; indicates whether to use Maximum Mean Discrepancy penalty
    reduction :  str; string indicating type of reduction to perform
    """

    # Initializer method
    def __init__(self, alpha=1.0 , beta=1.0, mmd=True, mmd_size=500, gamma=1.0):
        """
        Description
        -----------
        Loss initializer method.

        Parameters
        ----------
        alpha :  float;
        beta :  float;
        mmd :  bool; indicates whether to use Maximum Mean Discrepancy penalty
        mmd_size :  int;
        gamma :
        reduction :  str; string indicating type of reduction to perform

        Returns
        -------
        N/A
        """

        # Inherit from torch.nn.modules.loss._Loss()
        super(sVFAELoss, self).__init__()

        # Assign attributes
        self.alpha = alpha
        self.beta = beta
        self.mmd = mmd
        self.mmd_size = mmd_size
        self.gamma = gamma
        self.reduction = 'mean'

        # Create loss attributes
        # self.ce_loss = CrossEntropyLoss(reduction='mean')
        self.bce_loss = BCEWithLogitsLoss(reduction='mean')
        self.ce_loss = BCEWithLogitsLoss(reduction='mean')

    # Forward method
    def forward(self, x, s, y, outputs):
        """
        Description
        -----------
        Loss forward method.

        Parameters
        ----------
        x :  torch.Tensor(); batch tensor of primary variables to encode
        s :  torch.Tensor(); batch tensor of sensitive variables
        y :  torch.Tensor(); batch stens
        outputs :

        Returns
        -------
        loss :  torch.Tensor(); loss value
        """

        # Concatenate tensors
        # x, s, y = y_true['x'], y_true['s'], y_true['y']
        x_s = torch.cat([x, s], dim=-1)

        # Calculate batch label prediction loss
        supervised_loss = self.ce_loss(outputs['y_dec'], y)

        # Calculate batch x reconstruction loss
        recon_loss = self.bce_loss(outputs['x_dec'], x_s)

        # KL-divergence between z1 encoding and z1 decoding
        z1_kl_loss = self._kl_gaussian(mu_a=outputs['z1_enc_mu'],
                                       logvar_a=outputs['z1_enc_logvar'],
                                       mu_b=outputs['z1_dec_mu'],
                                       logvar_b=outputs['z1_dec_logvar'])

        # KL-divergence between z2 and prior
        zeros = zeros_like(outputs['z2_enc_mu'])
        z2_kl_loss = self._kl_gaussian(mu_a=outputs['z2_enc_mu'],
                                       logvar_a=outputs['z2_enc_logvar'],
                                       mu_b=zeros,
                                       logvar_b=zeros)

        # Sum loss terms
        loss = recon_loss + z1_kl_loss + z2_kl_loss + (self.alpha * supervised_loss)

        # MMD penalty
        if self.mmd:
            z1_enc = outputs['z1_enc']
            z1_s0, z1_s1 = self._separate_classes(x=z1_enc, s=s)
            if len(z1_s0) > 0:
                loss += self.beta * self._fast_mmd(z1_s0, z1_s1)

        # Return loss
        return loss

    # KL divergence method
    @staticmethod
    def _kl_gaussian(mu_a, logvar_a, mu_b, logvar_b):
        """
        Description
        -----------
        KL divergence between two gaussian distributions.

        Parameters
        ----------
        mu_a :  torch.Tensor(); distribution a mean values
        logvar_a :  torch.Tensor(); distribution a log-variance values
        mu_b :  torch.Tensor(); distribution b mean values
        logvar_b :  torch.Tensor(); distribution b log-variance values

        Returns
        -------
        kl_sum :  torch.Tensor(); batch tensor of KL-divergence values
        """

        # Calculate KL divergence
        multivariate_kl = 0.5 * (logvar_b - logvar_a - 1.0 + ((square(mu_a - mu_b) + exp(logvar_a)) / exp(logvar_b)))
        kl_sum = sum(multivariate_kl, dim=1).mean()

        # Return divergence
        return kl_sum

    # MMD penalty
    def _fast_mmd(self, s0, s1):
        """
        Description
        -----------
        Fast Maximum Mean Discrepancy method.

        Parameters
        ----------
        s0 :
        s1 :

        Returns
        -------
        """

        # Calculate fast MMD penalty
        mmd_pen = torch.norm(self._phi(s0).mean(dim=0) - self._phi(s1).mean(dim=0), 2)

        # Return penalty
        return mmd_pen

    # MMD kernel
    def _phi(self, x):
        """

        :return:
        """

        # W sampled from normal
        n_features = x.shape[-1]
        W = torch.randn((n_features, self.mmd_size), device=x.device)

        # b sampled from uniform
        b = torch.zeros((self.mmd_size,), device=x.device).uniform_(0, 2 * pi)

        # Calculate phi kernel
        phi_kernel = sqrt(2 / self.mmd_size) * cos((sqrt(2 / self.gamma) * (torch.mm(x, W))) + b)

        # Return phi kernel
        return phi_kernel

    # Separate sensitive classes
    @staticmethod
    def _separate_classes(x, s):
        """
        Description
        -----------


        Parameters
        ----------
        x :
        s :

        Returns
        -------
        x_s0 :
        x_s1 :
        """

        # Get indices of
        idx_s0 = (s == 0).nonzero()[:, 0]
        idx_s1 = (s == 1).nonzero()[:, 0]

        # Split sensitive according to sensitive class labels
        x_s0 = x[idx_s0]
        x_s1 = x[idx_s1]

        # Return sensitive class splits
        return x_s0, x_s1


# Unsupervised Variational Fair Autoencoder loss
class uVFAELoss(nn.modules.loss._Loss):
    """
    Description
    -----------
    Unsupervised Variational Fair Autoencoder loss class.

    Parameters
    ----------
    reduction :  str; string indicating type of reduction to perform

    Attributes
    ----------
    reduction :  str; string indicating type of reduction to perform
    """

    # Initializer method
    def __init__(self, reduction='mean'):
        """
        Description
        -----------
        Loss initializer method.

        Parameters
        ----------
        reduction :  str; string indicating type of reduction to perform

        Returns
        -------
        N/A
        """

        # Inherit from torch.nn.modules.loss._Loss()
        super(uVFAELoss, self).__init__()

        # Assign attributes
        self.reduction = reduction

    # Forward method
    def forward(self, input, target):
        """
        Description
        -----------
        Loss forward method.

        Parameters
        ----------
        input :  torch.Tensor(); batch tensor of inputs
        target :  torch.Tensor(); batch tensor of targets

        Returns
        -------
        loss :
        """


        # Return loss
        pass
        # return loss

    # KL divergence method
    def _kl_div(self, p, q):

        # Return divergence
        pass
        # return divergence

