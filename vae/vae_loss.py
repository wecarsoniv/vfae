# ----------------------------------------------------------------------------------------------------------------------
# FILE DESCRIPTION
# ----------------------------------------------------------------------------------------------------------------------

# File:  vae_loss.py
# Author:  Billy Carson
# Date written:  07-20-2020
# Last modified:  07-22-2020

"""
Description:  PyTorch implementation of the Variational Autoencoder proposed by Kingma and Welling, 2014.
"""


# ----------------------------------------------------------------------------------------------------------------------
# IMPORT MODULES
# ----------------------------------------------------------------------------------------------------------------------

# Import modules
import torch
import torch.nn as nn
from torch import sum, exp, square, zeros_like
from torch.nn import CrossEntropyLoss, BCEWithLogitsLoss, MSELoss


# ----------------------------------------------------------------------------------------------------------------------
# DEFINE CLASSES
# ----------------------------------------------------------------------------------------------------------------------

# Variational Autoencoder loss
class VAELoss(torch.nn.modules.loss._Loss):
    """
    Description
    -----------


    Parameters
    ----------


    Attributes
    ----------

    """

    # Initializer method
    def __init__(self):
        """
        Description
        -----------


        Parameters
        ----------


        Returns
        -------
        N/A
        """

        # Inherit from torch.nn.modules.loss._Loss
        super(VAELoss, self).__init__()

        # Create loss attributes
        # self.bce_loss = BCEWithLogitsLoss(reduction='mean')
        self.bce_loss = MSELoss(reduction='mean')
    
    # Forward method
    def forward(self, x, outputs):
        """
        Description
        -----------
        Loss forward method.

        Parameters
        ----------
        x :  torch.Tensor(); batch tensor of inputs
        outputs :  dict; dictionary of model outputs

        Returns
        -------
        loss :  torch.Tensor(); model loss
        """

        # Reconstruction loss
        recon_loss = self.bce_loss(outputs['x_dec'], x)

        # KL divergence
        kl_div = self._kl_div(mu_a=outputs['z_enc_mu'], logvar_a=outputs['z_enc_logvar'])

        # Sum losses
        loss = recon_loss + kl_div

        # Return loss
        return loss

    # KL divergence method
    @staticmethod
    def _kl_div(mu_a, logvar_a, mu_b=None, logvar_b=None):
        """
        Description
        -----------
        KL divergence method.

        Parameters
        ----------
        mu_a :  torch.Tensor(); distribution a mean values
        logvar_a :  torch.Tensor(); distribution a log-variance values
        mu_b :  torch.Tensor(); distribution b mean values
        logvar_b :  torch.Tensor(); distribution b log-variance values

        Returns
        -------
        kl_sum :  torch.Tensor(); batch tensor of KL-divergence values
        """

        # Create zeros tensors for prior
        if mu_b is None:
            mu_b = zeros_like(mu_a)
        if logvar_b is None:
            logvar_b = zeros_like(logvar_a)

        # Calculate KL divergence
        multivariate_kl = 0.5 * (logvar_b - logvar_a - 1.0 + ((square(mu_a - mu_b) + exp(logvar_a)) / exp(logvar_b)))
        kl_sum = sum(multivariate_kl, dim=1).mean()

        # Return divergence
        return kl_sum

