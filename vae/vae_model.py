# ----------------------------------------------------------------------------------------------------------------------
# FILE DESCRIPTION
# ----------------------------------------------------------------------------------------------------------------------

# File:  vae_model.py
# Author:  Billy Carson
# Date written:  07-20-2020
# Last modified:  07-22-2020

"""
Description:  PyTorch implementation of the Variational Autoencoder proposed by Kingma and Welling, 2014.
"""


# ----------------------------------------------------------------------------------------------------------------------
# IMPORT MODULES
# ----------------------------------------------------------------------------------------------------------------------

# Import modules
import torch
import torch.nn as nn


# ----------------------------------------------------------------------------------------------------------------------
# DEFINE CLASSES
# ----------------------------------------------------------------------------------------------------------------------

# Variational Autoencoder class
class VAE(nn.Module):
    """
    Description
    -----------
    PyTorch implementation of the supervised version of the Variational Autoencoder (Kingma and Welling, 2014).

    Parameters
    ----------
    input_size :  int; primary feature dimensionality
    latent_size :  int; latent variable dimensionality
    enc_hidden_size :  int; encoder hidden layer size
    dec_hidden_size :  int; decoder hidden layer size
    hidden_activation :  torch.nn.modules.activation; hidden layer output activation
    output_activation :  torch.nn.modules.activation; decoder output layer activation

    Attributes
    ----------
    input_size :  int; primary feature dimensionality
    latent_size :  int; latent variable dimensionality
    enc_hidden_size :  int; encoder hidden layer size
    dec_hidden_size :  int; decoder hidden layer size
    hidden_activation :  torch.nn.modules.activation; hidden layer output activation
    output_activation :  torch.nn.modules.activation; decoder output layer activation
    enc_fc_hidden :  torch.nn.Module(); input to hidden fully-connected layer
    enc_fc_mu :  torch.nn.Module(); hidden to mean fully-connected layer
    enc_fc_logvar :  torch.nn.Module(); hidden to log-variance fully-connected layer
    decoder :  torch.nn.Module(); model decoder
    """

    # Initializer method
    def __init__(self, input_size, latent_size=10, enc_hidden_size=20, dec_hidden_size=None,
                 hidden_activation=nn.ReLU(inplace=False), output_activation=nn.Sigmoid()):
        """
        Description
        -----------
        Model initializer method.

        Parameters
        ----------
        input_size :  int; primary feature dimensionality
        latent_size :  int; latent variable dimensionality
        enc_hidden_size :  int; encoder hidden layer size
        dec_hidden_size :  int; decoder hidden layer size
        hidden_activation :  torch.nn.modules.activation; hidden layer output activation
        output_activation :  torch.nn.modules.activation; decoder output layer activation

        Returns
        -------
        N/A
        """

        # Inherit from torch.nn.Module class
        super(VAE, self).__init__()

        # Assign attributes
        self.input_size = input_size
        self.latent_size = latent_size
        self.enc_hidden_size = enc_hidden_size
        self.dec_hidden_size = dec_hidden_size
        self.hidden_activation = hidden_activation
        self.output_activation = output_activation

        # Create encoder
        self.enc_fc_hidden = nn.Linear(in_features=input_size, out_features=enc_hidden_size)
        self.enc_fc_mu = nn.Linear(in_features=enc_hidden_size, out_features=latent_size)
        self.enc_fc_logvar = nn.Linear(in_features=enc_hidden_size, out_features=latent_size)

        # Create decoder
        if dec_hidden_size is None:
            self.decoder = nn.Linear(in_features=latent_size, out_features=input_size)
        else:
            self.decoder = nn.Sequential(nn.Linear(in_features=latent_size, out_features=dec_hidden_size),
                                         nn.Linear(in_features=dec_hidden_size, out_features=input_size))

    # Forward method
    def forward(self, x):
        """
        Description
        -----------
        Model forward method.

        Parameters
        ----------
        x :  torch.Tensor();

        Returns
        -------
        outputs :  dict; dictionary of model outputs
        """

        # Forward pass through encoder
        x = self.enc_fc_hidden(x)
        x = self.hidden_activation(x)
        z_mu = self.enc_fc_mu(x)
        z_logvar = self.enc_fc_mu(x)

        # Reparameterize
        z_enc = self._reparameterize(mu=z_mu, logvar=z_logvar)

        # Reconstruct inputs
        x_dec = self.decoder(z_enc)

        # Output activation function
        if self.output_activation is not None:
            x_dec = self.output_activation(x_dec)

        # Create dictionary of model outputs
        outputs = {
            'z_enc': z_enc,
            'x_dec': x_dec,
            'z_enc_mu': z_mu,
            'z_enc_logvar': z_logvar
        }

        # Return outputs
        # return z, mu, logvar
        return outputs

    # Encoder method
    def encode(self, x):
        """
        Description
        -----------
        Variational Autoencoder encoder network.

        Parameters
        ----------
        x :  torch.Tensor(); batch tensor of inputs to be encoded

        Returns
        -------
        z :  torch.Tensor(); batch tensor of latent encodings
        """

        # Forward pass through encoder
        x = self.enc_fc_hidden(x)
        x = self.hidden_activation(x)
        z_mu = self.enc_fc_mu(x)
        z_logvar = self.enc_fc_logvar(x)

        # Reparameterize
        z = self._reparameterize(mu=z_mu, logvar=z_logvar)

        # Return latent encoding
        return z

    # Reparameterization trick method
    @staticmethod
    def _reparameterize(mu, logvar):
        """
        Description
        -----------
        Reparameterization trick method.

        Parameters
        ----------
        mu :  torch.Tensor(); tensor of mean values
        logvar :  torch.Tensor; tensor of log-variance values

        Returns
        -------
        z :  torch.Tensor(); tensor of latent encoding values
        """

        # Reparameterization trick
        std = torch.exp(0.5 * logvar)
        epsilon = torch.randn_like(std)
        z = mu + (epsilon * std)

        # Return z
        return z

